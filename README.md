The Trove harvest is handled differently from the other feeds. The raw data is a series of gzipped CSV files, stored at:

https://console.aws.amazon.com/s3/buckets/huni-backups/trove-data/

The CSV files are processed into a SQLite database, and that in turn is used to populate the Solr server.

The SQLite database is a normalised version of the original Trove dataset. No
"transforms" have been applied. The transforms are coded into the db2solr
script. If the transforms need to be changed, then the db2solr script will need
to be modified, and then re-run as per above.

There's a cpanfile if you want to install the Perl dependencies locally, or a docker-compose file if you'd rather do it that way.

To recreate the database, download the gzipped CSV files, them gzipped, and run:

```sh
docker-compose run --rm trove createdb trove.db
docker-compose run --rm trove csv2db trove.db *.csv.gz
```

The then upload the data to solr, run:

```sh
docker-compose run --rm trove db2solr solr-host-ip:port
```
