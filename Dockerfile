FROM perl:5.30

WORKDIR /work
COPY cpanfile /root/

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      sqlite3 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists \
 && cpanm -q --installdeps /root/ \
 && rm -rf /root/.cpanm
