-- publication: id, name
-- article: id, date, source, title
-- person:  id, name
-- mention: article_id, person_id, snippet

PRAGMA foreign_keys = ON;

CREATE TABLE publication (
    id              INTEGER PRIMARY KEY,
    name            TEXT UNIQUE NOT NULL
);

CREATE TABLE article (
    id              TEXT PRIMARY KEY,
    date            TEXT NOT NULL,
    title           TEXT NOT NULL,
    publication_id  INTEGER NOT NULL,

    FOREIGN KEY(publication_id) REFERENCES publication(id)
);

CREATE TABLE person (
    id              TEXT PRIMARY KEY,
    name            TEXT UNIQUE NOT NULL
);

CREATE TABLE mention (
    article_id      TEXT NOT NULL,
    person_id       TEXT NOT NULL,
    context         TEXT NOT NULL,

    UNIQUE (article_id, person_id, context),

    FOREIGN KEY(article_id) REFERENCES article(id),
    FOREIGN KEY(person_id)  REFERENCES person(id)
);
